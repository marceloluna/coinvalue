﻿using CoinValue.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoinValue.Services
{
    public interface ICoinService
    {
        Task<List<string>> GetCoinsDescription();
        Task<decimal> ConvertCoin(string coinFrom, string coinTo);
        Task<List<Coin>> GetListBasedInSpecificCurrency(string coin);
    }
}
