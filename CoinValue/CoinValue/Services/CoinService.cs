﻿using CoinValue.Models;
using CoinValue.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms.Internals;

namespace CoinValue.Services
{
    [Preserve(AllMembers = true)]
    class CoinService : ICoinService
    {
        HTTPClient _httpClient;
        private List<Coin> _coins;
        private Coin _refCoin;
        public CoinService()
        {
            _httpClient = new HTTPClient();
            _coins = new List<Coin>();
            _refCoin = new Coin
            {
                Name = "USD",
                Value = 1
            };
        }

        public async Task<decimal> ConvertCoin(string coinFrom, string coinTo)
        {
            if (coinFrom != _refCoin.Name || _coins.Count == 0)
            {
                this._refCoin.Name = coinFrom;
                _coins = await GetListBasedInSpecificCurrency(coinFrom);
            }

            return _coins.First(x => x.Name == coinTo).Value;
        }

        public async Task<List<Coin>> GetListBasedInSpecificCurrency(string refCoin)
        {
            var coins = new List<Coin>();
            var response = (await _httpClient.Get($"https://api.exchangeratesapi.io/latest?base={refCoin }")).Split('{')[2].Split('}')[0].Split(',');

            response.ForEach(c =>
            {
                var currentCoin = c.Split(':');
                coins.Add(new Coin
                {
                    Name = Regex.Replace(currentCoin[0], @"[^A-Z]+", String.Empty),
                    Value = Convert.ToDecimal(currentCoin[1])
                });
            });

            return coins;
        }

        public async Task<List<string>> GetCoinsDescription()
        {
            var coinsDescription = new List<string>();
            var coinList = new List<Coin>();

            var response = (await _httpClient.Get($"https://api.exchangeratesapi.io/latest?base=USD")).Split('{')[2].Split('}')[0].Split(',');

            response.ForEach(c =>
            {
                var currentCoin = c.Split(':');
                coinsDescription.Add(Regex.Replace(currentCoin[0], @"[^A-Z]+", String.Empty));
            });

            return coinsDescription;
        }
    }
}
