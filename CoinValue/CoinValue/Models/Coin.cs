﻿using Xamarin.Forms.Internals;

namespace CoinValue.Models
{
    [Preserve(AllMembers = true)]
    public class Coin
    {
        public string Name { get; set; }
        public decimal Value { get; set; }
    }
}
