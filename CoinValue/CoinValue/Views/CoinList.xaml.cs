﻿using CoinValue.Services;
using CoinValue.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace CoinValue.Views
{
    [Preserve(AllMembers = true)]

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CoinList : ContentPage
    {
        public CoinList()
        {
            InitializeComponent();
            BindingContext = new CoinListViewModel(new CoinService());
        }
    }
}