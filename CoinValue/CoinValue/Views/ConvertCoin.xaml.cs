﻿using CoinValue.Services;
using CoinValue.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace CoinValue.Views
{
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConvertCoin : ContentPage
    {
        ConvertCoinViewModel _viewModel;
        public ConvertCoin()
        {
            InitializeComponent();
            BindingContext = _viewModel = new ConvertCoinViewModel(new CoinService());
        }
    }
}