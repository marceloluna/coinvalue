﻿using System.Net.Http;
using System.Threading.Tasks;
using Xamarin.Forms.Internals;

namespace CoinValue.Utils
{
    [Preserve(AllMembers = true)]
    public class HTTPClient
    {
        private HttpClient _client;
        public HTTPClient()
        {
            _client = new HttpClient();
        }
        public async Task<string> Get(string uri)
        {
            HttpResponseMessage httpResponse = await _client.GetAsync(uri);
            if (httpResponse.IsSuccessStatusCode)
            {
                return await httpResponse.Content.ReadAsStringAsync();
            }

            return "";
        }
    }
}
