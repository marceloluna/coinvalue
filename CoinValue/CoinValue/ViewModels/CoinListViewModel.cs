﻿using CoinValue.Models;
using CoinValue.Services;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms.Internals;

namespace CoinValue.ViewModels
{
    [Preserve(AllMembers = true)]
    class CoinListViewModel : BaseViewModel
    {
        private ICoinService _coinService;
        public CoinListViewModel(ICoinService coinService)
        {
            _coinService = coinService;
            Title = "List of Coins";
            Task.Run(async () =>
            {
                await LoadPicker();
                SelectedCoin = "USD";
            });
        }
        public CoinListViewModel()
        {

        }

        private ObservableCollection<Coin> coinList;
        public ObservableCollection<Coin> CoinList
        {
            get { return coinList; }
            set
            {
                coinList = value;
                OnPropertyChanged("CoinList");
            }
        }

        private ObservableCollection<string> coins;
        public ObservableCollection<string> Coins
        {
            get { return coins; }
            set
            {
                coins = value;
                OnPropertyChanged("Coins");
            }
        }

        private string selectedCoin;
        public string SelectedCoin
        {
            get { return selectedCoin; }
            set
            {
                selectedCoin = value;
                OnPropertyChanged("SelectedCoin");
                if (selectedCoin != null)
                {
                    Task.Run(async () => await LoadCoins(selectedCoin));
                }
            }
        }

        private async Task LoadCoins(string selectedCoin)
        {
            CoinList = new ObservableCollection<Coin>(await _coinService.GetListBasedInSpecificCurrency(selectedCoin));
        }

        private async Task LoadPicker()
        {
            Coins = new ObservableCollection<string>(await _coinService.GetCoinsDescription());
        }
    }
}
