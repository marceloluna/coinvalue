﻿using CoinValue.Services;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms.Internals;

namespace CoinValue.ViewModels
{
    [Preserve(AllMembers = true)]
    class ConvertCoinViewModel : BaseViewModel
    {
        private ICoinService _coinService;
        private List<string> _coins;
        private decimal _currentValueCoin = 1;
        public ConvertCoinViewModel(ICoinService coinService)
        {
            _coinService = coinService;
            Task.Run(async () => 
            {
                await LoadPickers();
                SelectedCoinFrom = "USD";
                SelectedCoinTo = "EUR";
            });

            CurrencyFrom = 100;
            Title = "Convert Coin";
        }

        private async Task LoadPickers()
        {
            _coins = await _coinService.GetCoinsDescription();
            Coins = new ObservableCollection<string>(_coins);
        }

        public ConvertCoinViewModel()
        {

        }

        private decimal currencyFrom;
        public decimal CurrencyFrom
        {
            get { return currencyFrom; }
            set
            {
                currencyFrom = value;
                SetConvert();
                OnPropertyChanged("CurrencyFrom");
            }
        }

        private void SetConvert()
        {
            CurrencyTo = currencyFrom * _currentValueCoin;
        }

        private decimal currencyTo;
        public decimal CurrencyTo
        {
            get { return currencyTo; }
            set
            {
                currencyTo = value;
                OnPropertyChanged("CurrencyTo");
            }
        }

        private ObservableCollection<string> coins;
        public ObservableCollection<string> Coins
        {
            get { return coins; }
            set
            {
                coins = value;
                OnPropertyChanged("Coins");
            }
        }

        private string selectedCoinFrom;
        public string SelectedCoinFrom
        {
            get { return selectedCoinFrom; }
            set
            {
                selectedCoinFrom = value;
                OnPropertyChanged("SelectedCoinFrom");
                if (selectedCoinFrom != null && selectedCoinTo != null)
                {
                    Task.Run(async () => await SetCurrentValueCoin());
                }
            }
        }

        private async Task SetCurrentValueCoin()
        {
            _currentValueCoin = await _coinService.ConvertCoin(selectedCoinFrom, selectedCoinTo);
            SetConvert();
            DescriptionCoin = $"1 { SelectedCoinFrom } is equals a {_currentValueCoin} { SelectedCoinTo}";
        }

        private string selectedCoinTo;
        public string SelectedCoinTo
        {
            get { return selectedCoinTo; }
            set
            {
                selectedCoinTo = value;
                OnPropertyChanged("SelectedCoinTo");
                if (selectedCoinFrom != null && selectedCoinTo != null)
                {
                    Task.Run(async () => await SetCurrentValueCoin());
                }
            }
        }

        private string descriptionCoin;
        public string DescriptionCoin
        {
            get { return descriptionCoin; }
            set
            {
                descriptionCoin = value;
                OnPropertyChanged("DescriptionCoin");
            }
        }
    }
}
