﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using CoinValue.Services;
using CoinValue.Views;

namespace CoinValue
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            //DependencyService.Register<>();
            MainPage = new AppShell();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
